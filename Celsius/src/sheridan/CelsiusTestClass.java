package sheridan;

import static org.junit.Assert.*;


import sheridan.Celsius;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


import org.junit.Test;

public class CelsiusTestClass {

	

	@Test
	public void testIsValidConversionRegular() {
		int isValidConversion = Celsius.fromFahrenheit(32);
		assertTrue("Invalid to Celsius Conversion!", isValidConversion ==0);
	}

	@Test(expected=NumberFormatException.class)
	public void testIsValidConversionException() {
		int isValidConversion = Celsius.fromFahrenheit(-32);
		fail("Invaild value!");
		//assertFalse("Invalid to Celsius Conversion!", isValidConversion ==0);
	}
	
	@Test
	public void testIsValidConversionBoundaryIn() {
		int isValidConversion = Celsius.fromFahrenheit(32);
		assertTrue("Invalid to Celsius Conversion!", isValidConversion ==0);
	}
	

	@Test(expected=NumberFormatException.class)
	public void testIsValidConversionBoundaryOut() {
		int isValidConversion = Celsius.fromFahrenheit(-32);
		fail("Invaild value!");
	}

}
